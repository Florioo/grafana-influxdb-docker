**Note:**
This container is created to accomodate local instances of grafana an influxdb, some modfications were made to increase the convenience/ease of use but impacting security. It is not recommended to use this to deploy a server, only use it for local deployments.

# Installation
## Windows
**Prerequisites**

Install [Docker Desktop](https://www.docker.com/products/docker-desktop)

_**Note:** Commercial use of Docker Desktop in larger enterprises (more than 250 employees OR more than $10 million USD in annual revenue) now requires a paid subscription. The grace period for those that will require a paid subscription ends on January 31, 2022_


Free & open source alternative, install:

[Rancher Desktop](https://rancherdesktop.io/)

For a manual installation:

* [WSL2](https://docs.microsoft.com/en-us/windows/wsl/install)
* [Docker-Engine](https://docs.docker.com/engine/install/)
* [Docker-compose](https://docs.docker.com/compose/install/)

**Starting the service**

* Click the run - detached.bat file to start the service in the **background**
* Click the run.bat file to start to start the service with a cmd window that stays open


## Linux
**Prerequisites**

Install

* [Docker-Engine](https://docs.docker.com/engine/install/)
* [Docker-compose](https://docs.docker.com/compose/)

**Starting the service**

Open terminal in the folder, run

`docker-compose -f "docker-compose.yml" -p "Grafana_influxdb" up -d`


# Information
When executing the docker compose, the 2 services will be started

Also a "data" folder will also be generated in this directory, this folder contains al the persistent data of grafana and influxdb.

## Influxdb
Influxdb on [localhost:8086](http://localhost:8086)

```
Username: admin
Password: Verhaert
Org:      Verhaert
Admin token: BKLJSDkzg78Ha_-v0FU9bgUh829uVOU6mCETnhiqW3KG40T58c5SOgv5inzyhfqsmmGi0wcuzCWilCRUxrs0ug==
```

## Grafana
Grafana on [localhost:3000](http://localhost:3000)

No credentials are needed, login is bypassed for convenience
